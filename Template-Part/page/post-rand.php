<?php
/**
 * @package Taplog
 * This Template For Displaying List Post
 * This Theme for BLog Site.
 */
?>

<?php
$ranpost = new WP_Query(array(
    'post_type'      => 'post',
    'post_status'    => 'publish',
    'orderby'        => 'rand',
    'posts_per_page'  => 4
));
if($ranpost->have_posts()): while($ranpost->have_posts()): $ranpost->the_post();
    ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class('post-entry-horzontal'); ?>>
        <div class="row b">
            <div class="thumbnail col-3" style="background: url('<?php echo esc_url(get_the_post_thumbnail_url())?>') center /cover">

            </div>
            <div class="meta-data col-9">
                <div class="post-meta">
                <ul class="post-meta-data">
                    <li class="author mr-2">
                        <?php echo get_avatar(get_the_author_meta('ID'),32);?>
                        <?php the_author()?>
                    </li>&bullet;
                    <li class="mr-2">
                        <?php the_time('M j,Y')?>
                    </li> &bullet;
                    <li class="ml-2">
                    <div class="com"><img src="<?php echo get_theme_file_uri('images/chat.svg')?>" alt="commenticon">
                        <?php comments_number('0','one','%')?>
                    </div>
                    </li>
                </ul>
                </div>
                <h2 class="t"><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
            </div>
        </div>
    </div>

<?php endwhile; wp_reset_postdata(); endif;?>
<!-- END post -->

