<?php
/**
 * @package Taplog
 * The template for displaying Comments.
 * The area of the page that contains comments and the comment form.
 */
if (post_password_required()) return;
?>
<div id="comments" class="comments-area pt-5">
    <?php if (have_comments()) : ?>
    <h2 class="mb-5">
        <?php printf(_nx('1 Comment', '%1$s Comment', get_comments_number(), 'comments title', 'taplog'),
number_format_i18n(get_comments_number()), '<span>'. get_the_title() . '</span>');
?>
    </h2>
    <ul class="comment-list">
        <?php wp_list_comments(array('walker'=> new Bootstrap_Comment_Walker(),
'short_ping'=> true,
));
?>
    </ul>

        <?php // Are there comments to navigate through?
if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
        <nav class="navigation comment-navigation" role="navigation">
            <h4 class="screen-reader-text section-heading">
                <?php _e('See Comments', 'taplog');
?>
            </h4>
            <div class="nav-previous">
                <?php previous_comments_link(__('&larr; Older Comments', 'taplog'));
?>
            </div>
            <div class="nav-next">
                <?php next_comments_link(__('Newer Comments &rarr;', 'taplog'));
?>
            </div>
        </nav>
       
            <?php endif; // Check for comment navigation ?>

            <?php if ( ! comments_open() && get_comments_number()) : ?>
            <p class="no-comments">
                <?php _e('Comments are closed.', 'taplog');
?>
            </p>
            <?php endif;
?>
            <?php endif; // have_comments() ?>

            <div class="comment-form-wrap pt-5">
                <?php $comment_args=array('comment_notes_before'=> '<p class="comment-notes">'. __('Thanks for choosing to leave a comment. Please keep in mind that all comments are moderated according to our comment policy, and your email address will NOT be published. Please Do NOT use keywords in the name field. Let\'s have a personal and meaningful conversation.', 'taplog') . (' *'? " monenai" : '') . '</p>',
'title_reply'=> __('Thanks To Choose Leave a Reply', 'taplog'),
'class_form'=> 'p-5 bg-light',
'title_reply_before'=> '<h3 class="mb-5">',
'title_reply_after'=> '</h3>',
'label_submit'=> "Sent Comment"
);
comment_form($comment_args);
?>
            </div>
</div>