<?php
/**
 * @package Taplog
 * This Template For Extra Future Add.
 * This Theme for BLog Site.
 */

//Remove Theme Website Input Field Form Comment Form
function remove_comment_fields($fields) {
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields','remove_comment_fields');

//Log In Error Hint Generate Stop
function login_error_hint_my_site(){
    return 'Something is wrong! Plz Try aging';
}
add_filter( 'login_errors', 'login_error_hint_my_site' );

// for view count function

function count_post_visits() {
	if( is_single() ) {
		global $post;
		$views = get_post_meta( $post->ID, 'my_post_viewed', true );
		if( $views == '' ) {
			update_post_meta( $post->ID, 'my_post_viewed', '1' );	
		} else {
			$views_no = intval( $views );
			update_post_meta( $post->ID, 'my_post_viewed', ++$views_no );
		}
	}
}
add_action( 'wp_head', 'count_post_visits' );