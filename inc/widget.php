<?php
/**
 * @package Taplog
 * This Template For Displaying Widget Area.
 * This Theme for BLog Site.
 */

// Register Sidebars
function Sidebar() {

    $sidebar = array(
        'id'            => 'sidebar',
        'class'         => 'right-side',
        'name'          => __( 'Left Sidebar', 'taplog' ),
        'description'   => __( 'add your widget', 'taplog' ),
        'before_title'  => '<h3 class="heading">',
        'after_title'   => '</h3>',
    );
    register_sidebar( $sidebar );

    $footer1 = array(
        'id'            => 'footer1',
        'class'         => 'footer1',
        'name'          => __( 'Footer Area1', 'taplog' ),
        'description'   => __( 'add your footer widget', 'taplog' ),
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        'before_widget' => '<div class="post-entry-sidebar">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $footer1 );

    $footer2 = array(
        'id'            => 'footer2',
        'class'         => 'footer2',
        'name'          => __( 'Footer Area 2', 'taplog' ),
        'description'   => __( 'add your footer widget', 'taplog' ),
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        'before_widget' => '<div class="post-entry-sidebar">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $footer2 );

    $footer3 = array(
        'id'            => 'footer3',
        'class'         => 'footer3',
        'name'          => __( 'Footer Area 3', 'taplog' ),
        'description'   => __( 'add your footer widget', 'taplog' ),
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        'before_widget' => '<div class="post-entry-sidebar">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $footer3 );

}
add_action( 'widgets_init', 'Sidebar' );