<?php
/**
* @package Taplog
* This Template For Displaying home Page.
* This Theme for BLog Site.
*/
get_header();
?>

<section class="site-section py-sm">
    <div class="container">
        <div class="row">
            <?php if(is_home()):?>
            <div class="col-md-6">
                <h2 class="mb-4 title">Latest Posts</h2>
            </div>
            <?php endif;?>
        </div>
        <div class="row blog-entries">
            <div class="col-md-12 col-lg-8 main-content">
                <div class="row">
                    <?php get_template_part('Template-Part/page/post')?>
                </div>
                <?php $paginatinos = array(
                'prev_text'          => __('Previews','taplog'),
                'next_text'          => __('Next','taplog'),
                );
                ?>
                <div class="row mt-5">
                    <div class="col-md-12 text-center">
                        <div id="page-pagination">
                            <?php echo paginate_links($paginatinos); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4 sidebar">
                <?php get_sidebar()?>
            </div>
        </div>
    </div>
</section>
<?php get_footer( )?>